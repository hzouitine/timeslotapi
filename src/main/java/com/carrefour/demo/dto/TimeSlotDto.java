package com.carrefour.demo.dto;

import com.carrefour.demo.entities.DeliveryModeCustomer;
import com.carrefour.demo.entities.DeliveryModeEnum;
import com.carrefour.demo.entities.TimeSlot;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.core.convert.converter.Converter;
import java.time.LocalDate;

@Data
public class TimeSlotDto {

    private LocalDate day;
    private Integer hourFrom;
    private Integer hourTo;
    private String deliveryMode;


    public TimeSlot convert() {
        var timeSlot = new TimeSlot();
        timeSlot.setHourFrom(this.getHourFrom());
        timeSlot.setHourTo(this.getHourTo());
        timeSlot.setTheDay(this.getDay());
        return timeSlot;
    }
}
