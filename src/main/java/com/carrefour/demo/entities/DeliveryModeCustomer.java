package com.carrefour.demo.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class DeliveryModeCustomer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private DeliveryMode deliveryMode;

    @ManyToOne
    private Customer customer;

    //TODO : Gerer la contrainte customer / delivery mode
}
