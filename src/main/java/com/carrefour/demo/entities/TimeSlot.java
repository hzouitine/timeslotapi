package com.carrefour.demo.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Data
public class TimeSlot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    DeliveryModeCustomer deliveryModeCustomer;

    @Column
    private LocalDate theDay;

    @Column
    private Integer hourFrom;

    @Column
    private Integer hourTo;

}
