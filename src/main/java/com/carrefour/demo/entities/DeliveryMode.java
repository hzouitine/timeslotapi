package com.carrefour.demo.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class DeliveryMode {

    @Id
    @Enumerated(EnumType.STRING)
    private DeliveryModeEnum id;

    @OneToMany(mappedBy = "deliveryMode", cascade = CascadeType.ALL)
    private List<DeliveryModeCustomer> deliveryModeCustomers;
}
