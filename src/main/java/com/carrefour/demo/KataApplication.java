package com.carrefour.demo;

import com.carrefour.demo.entities.Customer;
import com.carrefour.demo.entities.DeliveryMode;
import com.carrefour.demo.entities.DeliveryModeCustomer;
import com.carrefour.demo.entities.DeliveryModeEnum;
import com.carrefour.demo.repositories.DeliveryModeRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class KataApplication {


	public static void main(String[] args) {
		var ctx = SpringApplication.run(KataApplication.class, args);
	}

}
