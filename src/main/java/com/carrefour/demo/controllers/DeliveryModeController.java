package com.carrefour.demo.controllers;

import com.carrefour.demo.entities.DeliveryMode;
import com.carrefour.demo.entities.DeliveryModeEnum;
import com.carrefour.demo.services.DeliveryModeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/deliveryMode")
public class DeliveryModeController {

    @Autowired
    private DeliveryModeService deliveryModeService;

    @PostMapping()
    void selectDeliveryMode(@RequestBody DeliveryModeEnum deliveryMode){
        deliveryModeService.selectDeliveryMode(deliveryMode);
    }
}
