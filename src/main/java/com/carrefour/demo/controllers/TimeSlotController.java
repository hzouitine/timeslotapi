package com.carrefour.demo.controllers;

import com.carrefour.demo.dto.TimeSlotDto;
import com.carrefour.demo.entities.DeliveryModeEnum;
import com.carrefour.demo.services.TimeSlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/timeSlot")
public class TimeSlotController {

    @Autowired
    TimeSlotService timeSlotService;

    @PostMapping()
    void selectTimeSlot(@RequestBody TimeSlotDto timeSlotDto) throws Exception {
        timeSlotService.selectTimeSlot(timeSlotDto);
    }

}
