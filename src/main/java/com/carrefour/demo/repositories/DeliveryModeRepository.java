package com.carrefour.demo.repositories;

import com.carrefour.demo.entities.DeliveryMode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryModeRepository extends CrudRepository<DeliveryMode, String> {
}
