package com.carrefour.demo.repositories;

import com.carrefour.demo.entities.DeliveryModeCustomer;
import com.carrefour.demo.entities.DeliveryModeEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryModeCustomerRepository extends CrudRepository<DeliveryModeCustomer, Long> {

    @Query("SELECT dmc FROM DeliveryModeCustomer dmc WHERE dmc.customer.id = :customerId and dmc.deliveryMode.id = :deliveryMode")
    DeliveryModeCustomer findByCustomerIdAAndDeliveryMode(Long customerId, DeliveryModeEnum deliveryMode);
}
