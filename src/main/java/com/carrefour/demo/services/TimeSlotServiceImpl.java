package com.carrefour.demo.services;

import com.carrefour.demo.dto.TimeSlotDto;
import com.carrefour.demo.entities.DeliveryModeEnum;
import com.carrefour.demo.entities.TimeSlot;
import com.carrefour.demo.repositories.DeliveryModeCustomerRepository;
import com.carrefour.demo.repositories.TimeSlotRepository;
import org.springframework.stereotype.Service;

@Service
public class TimeSlotServiceImpl implements TimeSlotService{

    private DeliveryModeCustomerRepository deliveryModeCustomerRepository;
    private UserService userService;
    private TimeSlotRepository timeSlotRepository;

    TimeSlotServiceImpl(DeliveryModeCustomerRepository deliveryModeCustomerRepository, UserService userService, TimeSlotRepository timeSlotRepository){
        this.deliveryModeCustomerRepository = deliveryModeCustomerRepository;
        this.userService = userService;
        this.timeSlotRepository = timeSlotRepository;
    }

    @Override
    public void selectTimeSlot(TimeSlotDto timeSlotDto) throws Exception {
        Long userId = userService.getConnectedUser().getId();
        var deliveryModeCustomer = deliveryModeCustomerRepository.findByCustomerIdAAndDeliveryMode(userId, DeliveryModeEnum.valueOf(timeSlotDto.getDeliveryMode()));
        if(deliveryModeCustomer == null){
            //TODO : customiser l'exception et passer par un Controller Advice
            throw new Exception("No delivery mode is selected for the time slot");
        }
        var timeSlot = timeSlotDto.convert();
        timeSlot.setDeliveryModeCustomer(deliveryModeCustomer);
        timeSlotRepository.save(timeSlot);


    }
}
