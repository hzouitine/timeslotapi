package com.carrefour.demo.services;

import com.carrefour.demo.entities.Customer;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    // TODO : Classe pour récuperer le client connécte, à implementer normalement avec Spring Security
    Customer getConnectedUser(){
        var customer = new Customer();
        customer.setId(1L);
        return customer;
    }
}
