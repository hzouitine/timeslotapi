package com.carrefour.demo.services;

import com.carrefour.demo.entities.DeliveryMode;
import com.carrefour.demo.entities.DeliveryModeCustomer;
import com.carrefour.demo.entities.DeliveryModeEnum;
import com.carrefour.demo.repositories.DeliveryModeCustomerRepository;
import org.springframework.stereotype.Service;

@Service
public class DeliveryModeServiceImpl implements DeliveryModeService{

    private DeliveryModeCustomerRepository deliveryModeCustomerRepository;

    private UserService userService;

    DeliveryModeServiceImpl(DeliveryModeCustomerRepository deliveryModeCustomerRepository, UserService userService){
        this.deliveryModeCustomerRepository = deliveryModeCustomerRepository;
        this.userService = userService;
    }

    @Override
    public void selectDeliveryMode(DeliveryModeEnum deliveryModeEnum) {
        var deliveryMode = new DeliveryMode();
        deliveryMode.setId(deliveryModeEnum);

        var deliveryModeCustomer = new DeliveryModeCustomer();
        deliveryModeCustomer.setDeliveryMode(deliveryMode);
        deliveryModeCustomer.setCustomer(userService.getConnectedUser());
        deliveryModeCustomerRepository.save(deliveryModeCustomer);
    }
}
