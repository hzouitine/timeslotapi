package com.carrefour.demo.services;

import com.carrefour.demo.entities.DeliveryModeEnum;

public interface DeliveryModeService {
    void selectDeliveryMode(DeliveryModeEnum deliveryMode);
}
