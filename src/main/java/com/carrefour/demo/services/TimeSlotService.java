package com.carrefour.demo.services;

import com.carrefour.demo.dto.TimeSlotDto;

public interface TimeSlotService {

    void selectTimeSlot(TimeSlotDto timeSlotDto) throws Exception;
}
