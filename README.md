# Carrefour Java Kata - Livraison

## MVP : Cet exercice est réalisé en environ 2 heures.

### User Stories :
1. En tant que client, je peux choisir mon mode de livraison parmi les options suivantes : `DRIVE`, `DELIVERY`, `DELIVERY_TODAY`, `DELIVERY_ASAP`.
2. En tant que client, je peux choisir mon jour et mon créneau horaire pour la livraison.

### Comment exécuter le projet :
- Utiliser Maven directement : `mvn spring-boot:run`
- Compiler et générer un JAR (`mvn clean package`) puis exécuter le JAR directement avec Java.

### Reste à faire / Fonctionnalités bonus :
- Sécuriser l'API : Utiliser Spring Security et JWT.
- Documentation de l'API REST : Utiliser Swagger UI.
- Proposer une solution de mise en cache : Utiliser Spring Boot Cache.
- Proposer une solution de streaming de données : Ajouter un Producer Apache Kafka en utilisant les dépendances kafka et spring kafka.
- CI/CD : Utiliser Gitlab CI pour la partie qualité du code (Sonar) et le build. Créer une image Docker : Utiliser un Dockerfile ou un plugin Maven. Déploiement : Déployer sur Kubernetes en créant des manifests pour notre application.

